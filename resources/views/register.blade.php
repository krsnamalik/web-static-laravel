<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="post">
		@csrf
		<label for="fname">First name:</label><br>
		<input type="text"name="fname"><br>
		<label for="lname">Last name:</label><br>
		<input type="text" name="lname">

		<p>Gender :</p>
		<input type="radio" id="male" name="gender" value="male">
		<label for="male">Male</label><br>
		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label><br>
		<input type="radio" id="other" name="gender" value="other">
		<label for="other">Other</label>

		<p>Nationally</p>
		<select>
			<option>Indonesia</option>
			<option>Singapore</option>
			<option>Malaysian</option>
			<option>Australian</option>
		</select>
		<p>Language Spoken</p>
		<input type="checkbox" name="00">Bahasa indonesia<br>
		<input type="checkbox" name="00">English<br>
		<input type="checkbox" name="00">Other
		<p>Bio</p>
		<textarea cols="35" rows="12"></textarea><br>
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>